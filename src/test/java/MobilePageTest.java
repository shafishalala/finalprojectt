import components.MainHeaderComponent;
import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.MobilePage;
//import pages.Cards;
import pages.MainPage;
import pages.MobilePage;

//import static data.CardData.Kart1;

public class MobilePageTest {
    private WebDriver driver;

    @BeforeMethod
    public void init () throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }

    @Test
    public void headerofmobilebank(){
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .clickBankFull();
        new MobilePage(driver)
                .open("/az/ferdi/bank-24-7");
        new MobilePage(driver)
                .clickMobileBank();
        new MobilePage(driver)
                .open("/az/ferdi/bank-24-7/abb-mobile");
//        new MobilePage(driver)
//                .newwindow();
    }


    @AfterMethod
    public void close () {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }}