import components.InterIpoComponent;
import components.IpoComponent;
import components.MainHeaderComponent;
import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.MainPage;

public class IpoPageTest{
    private WebDriver driver;

    @BeforeMethod
    public void init() throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }

    @Test
    public void checkButtonisSelected() {
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseIpoteca()
                .pageHeaderShouldBeSameAs("İpoteca");
        new InterIpoComponent(driver)
                .chooseOrder();
        new IpoComponent(driver)
                .checkXeyrChosen();
    }

    @AfterMethod
    public void close() {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }
}
