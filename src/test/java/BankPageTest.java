import components.BankComponent;
import components.CardComponent;
import components.MainHeaderComponent;
import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.MainPage;

import java.time.Duration;
import java.util.Set;

public class BankPageTest {
    private WebDriver driver;

    @BeforeMethod
    public void init() throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();

    }

    @Test
    public void checkMobilePageHeader() {
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseBank();
        String pageTitle = driver.getTitle();
        Assert.assertEquals(pageTitle, "Bank 24/7");
        new BankComponent(driver)
                .checkMobilePageHeader("ABB mobile – Sadə və sürətli");

    }

    @AfterMethod
    public void close() {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }
}
