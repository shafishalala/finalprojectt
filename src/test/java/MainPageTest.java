

import components.MainHeaderComponent;
import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.MainPage;

public class MainPageTest {
    private WebDriver driver;

    @BeforeMethod
    public void init() throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }

    @Test
    public void MainPageTitle(){
        new MainPage(driver)
                .open("");
        String pageTitle = driver.getTitle();
        Assert.assertEquals(pageTitle, "ABB - Müasir, Faydalı, Universal");

    }

    @Test
    public void CardPageTitle() {
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseCard();
        String pageTitle = driver.getTitle();
        Assert.assertEquals(pageTitle, "Online Kart Sifarişi - Debet və Kredit kart- ABB Bank Kartları");

    }
    @Test
    public void NotChoosed() {
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)

                .chooseIpoteca();
        //  .checkNotChosed();
    }
    @Test //4
    public void headerofmobilebank(){
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .clickBankFull();


//        @AfterMethod
//        public void close() {
//            if (this.driver != null) {
//                this.driver.close();
//                //  this.driver.quit();
//            }
//        }
    }}







