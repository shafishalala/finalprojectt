package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class MobilePage extends AbsBasePage {

    public MobilePage(WebDriver driver) {
        super(driver);}
    @FindBy(css="li>span>a[href=\"https://abb-bank.az/az/ferdi/bank-24-7\"]")
    private WebElement Bank7;
    public MobilePage clickMobile() {
        Bank7.click();
        return new MobilePage(driver);
    }
    @FindBy(css="#js-hover-link > span:nth-child(1)")
    private WebElement MobilBanking;
    public MobilePage clickMobileBank(){
        MobilBanking.click();
        return new MobilePage(driver);

    }
}