package driver;

import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
//import static jdk.javadoc.internal.doclets.formats.html.markup.HtmlStyle.exceptions;

public interface IDriverFactory {
    WebDriver getDriver() throws DriverNotSupportedException;
}
