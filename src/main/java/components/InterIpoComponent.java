package components;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.IpoPage;

public class InterIpoComponent extends AbsBaseComponent {
    public InterIpoComponent(WebDriver driver) {
        super(driver);
    }
    @FindBy(css = "body > section:nth-child(8) > div > div > div > div > div:nth-child(1) > div > div.col-lg-7.pb-3.pb-lg-0 > div > div.d-flex.flex-column.flex-lg-row.flex-md-row.align-items-center.pos-xl-absolute.bottom-0 > a.btn.bg-1885d3.radius-5.border.border-1885d3.border-1\\.\\\\5.fs-15.fw-500.color-white.w-fit-content.w-md-100.w-100.mr-lg-3.mb-lg-0.mb-md-0.mb-2.mr-md-2.mr-0")
    private WebElement order;

    public IpoPage chooseOrder() {
        order.click();
        return new IpoPage(driver);
    }
}


