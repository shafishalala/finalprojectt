package components;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import pages.CardPage;

import java.util.Set;

public class CardComponent extends AbsBaseComponent {
    public CardComponent (WebDriver driver){
        super(driver);
    }
    @FindBy(css = "div.p-2.h-100p.d-flex.flex-column.align-items-start>a>figure>img[alt='TamKart MasterCard Standard PayPass - Debet']")
    private WebElement Mastercard;

    public CardPage chooseCard(){
        Mastercard.click();
        return new CardPage(driver);
    }
}
