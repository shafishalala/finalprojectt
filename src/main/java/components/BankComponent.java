package components;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import pages.BankPage;
import pages.CardPage;

import java.util.Set;

public class BankComponent extends AbsBaseComponent {
    public BankComponent(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#js-hover-link > span:nth-child(1) > a")
    private WebElement Mobilbank;
    @FindBy(css="h1")
    private WebElement header;


    public BankPage chooseMobilBank() {
        Mobilbank.click();
        return new BankPage(driver);
    }


    public void checkMobilePageHeader(String header) {
        chooseMobilBank();
        String mainWindowHandle = driver.getWindowHandle();
        Set<String> windowHandles = driver.getWindowHandles();
        for (String childWindow : windowHandles) {
            if (!mainWindowHandle.equalsIgnoreCase(childWindow)) {
                driver.switchTo().window(childWindow);
            }
            Assert.assertTrue(this.header.isDisplayed(), "Header is not displayed in the new window");


        }
    }
}
