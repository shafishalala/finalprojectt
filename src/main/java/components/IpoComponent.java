package components;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import pages.BankPage;
import pages.CardPage;

import java.util.Set;

public class IpoComponent extends AbsBaseComponent {
    public IpoComponent(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#from_abb_no")
    private WebElement xeyrInput;

    public void checkXeyrChosen() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click()", xeyrInput);
        Assert.assertTrue(xeyrInput.isSelected(), "Xeyr input is not selected");
    }
}

