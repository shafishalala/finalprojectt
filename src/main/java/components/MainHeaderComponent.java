package components;


import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import pageobject.AbsPageObject;
import pages.BankPage;
import pages.CardPage;
import pages.IpoPage;
import pages.MobilePage;

public class MainHeaderComponent extends AbsBaseComponent {
    public MainHeaderComponent(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "li>span>a[href=\"https://abb-bank.az/az/ferdi/kartlar\"]")
    private WebElement card;

    public CardPage chooseCard() {
        card.click();
        return new CardPage(driver);
    }


    @FindBy(css = "li>span>a[href=\"https://abb-bank.az/az/ferdi/bank-24-7\"]")
    private WebElement bank;

    public BankPage chooseBank() {
        bank.click();
        return new BankPage(driver);
    }

    @FindBy(css = "li>span>a[href='https://abb-bank.az/az/ferdi/kreditler/ipoteka-kreditleri']")
    private WebElement Ipoteca;

    public IpoPage chooseIpoteca() {
        Ipoteca.click();
        return new IpoPage(driver);
    }
    @FindBy(css="li>span>a[href=\"https://abb-bank.az/az/ferdi/bank-24-7\"]")
    private WebElement BankFull;
    public MobilePage clickBankFull() {
        BankFull.click();
        return new MobilePage(driver);
    }
}


//    @FindBy(css = "li>span>a[href='https://abb-bank.az/az/ferdi/kreditler/ipoteka-kreditleri']")
//    private WebElement Ipoteca;
//    private WebElement Not;
//
//    public IpoPage chooseIpoteca() {
//        Ipoteca.click();
//        return new IpoPage(driver);
//    }
//
//    @FindBy(css = "#from_abb_no")
//    private WebElement NotInput;
//    public AbsPageObject checkNotChosen() {
//
//    public IpoPage checkNotChosen() {
//        Not.click();
//        JavascriptExecutor js = (JavascriptExecutor) driver;
//        js.executeScript("arguments[0].click()", Not);
//        Assert.assertTrue(Not.isSelected(), "Not input is not selected");
//    }
//}}



